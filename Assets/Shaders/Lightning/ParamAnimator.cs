﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParamAnimator : MonoBehaviour
{
	[SerializeField]
	private float min_value = 0.0f;
    
    [SerializeField]
    private float max_value = 1.0f;

    [SerializeField]
    private string param_name;

    [SerializeField]
    private float animation_speed = 1.0f;

    [SerializeField]
    private bool remap_to_zero_one = true;
    
    [SerializeField]
    private bool do_even_reset = false;

    Renderer m_renderer;

    void Start()
    {
        m_renderer = GetComponent<Renderer> ();
    }

    // Update is called once per frame
    void Update()
    {
        if(do_even_reset && Time.time % 2.0 > 1.0)
            return;
            
        float a = Mathf.Cos(animation_speed * Time.time);
        float factor = remap_to_zero_one ?  0.5f * (a + 1) : a;
        m_renderer.material.SetFloat(param_name, Mathf.Lerp(min_value, max_value, factor));
    }
}
