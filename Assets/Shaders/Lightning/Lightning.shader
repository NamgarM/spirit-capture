﻿Shader "CoreMission/Lightning"
{
    Properties
    {
        _Value("Value", float) = 0.01
        _Threshold("Threshold", float) = 0.01
        _RotationSpeed("Rotation Speed", Range (1.0, 10.0)) = 1.0
    }
    SubShader
    {
        Cull Off 
        Tags { "RenderType"="Opaque" }
        LOD 100
 
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
             
            #include "UnityCG.cginc"
 
            struct appdata
            {
                float4 position : POSITION;
                float2 uv     : TEXCOORD0;
                fixed4 color  : COLOR0;
            };
 
            struct v2f
            {
                float2 uv     : TEXCOORD0;
                float4 position : SV_POSITION;
                fixed4 color  : COLOR0;
            };
 
            float _Threshold;
            float _Value;
            float _RotationSpeed;
 
            v2f vert (appdata v)
            {
                v2f o;
                o.uv = v.uv;
                // rotation
                float angle = _Time.y * _RotationSpeed;

                float3x3 R = {cos(angle), -sin(angle), 0,
                              sin(angle), cos(angle),  0,
                              0,              0,       1 };

                v.position = float4(mul(R, v.position.xyz), 1);

                o.position = UnityObjectToClipPos(v.position);

                float d = v.color.r - _Value;
                float factor = 1.0 - step(_Threshold, abs(d));
                o.position *= factor;
                o.color = d > 0 ? fixed4(0, 0, 0, 1) : fixed4(1, 1, 1, 1);
                //o.color = v.color + (factor > 0 ? fixed4(0, 1, 0, 0) : fixed4(0, 0, 0, 0));

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return i.color;
            }

            ENDCG
        }
    }
}