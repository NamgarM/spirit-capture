using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningShaderAnimation : MonoBehaviour
{
    [SerializeField]
    private float minVal = -0.2f;

    [SerializeField]
    private float maxVal = 1.2f;

    [SerializeField]
    private string shaderParameterName;

    [SerializeField]
    private float animationSpeed = 3.0f;
    [SerializeField]
    private bool remap_to_zero_one = true;

    [SerializeField]
    private bool do_even_reset = false;

    private Renderer renderer;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (do_even_reset && Time.time % 2.0 > 1.0)
            return;

        float animationTime = Mathf.Cos(animationSpeed * Time.time);
        float factor = remap_to_zero_one ? 0.5f * (animationTime + 1) : animationTime;
        renderer.material.SetFloat(shaderParameterName,
            Mathf.Lerp(minVal, maxVal, factor));
    }
}
