Shader "Custom/Spirits/SpiritSurfaceShader"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_SpecularMap("Metallic Map", 2D) = "white" {}
		_OcclusionMap("Occlusion Map", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_RimColor("Rim Color", Color) = (0.26,0.19,0.16,0.0)
		_RimPower("Rim Power", Range(0.1,2.0)) = 3.0
		_Noise("Noise", 2D) = "white" {}
		_Speed("Speed", Range(-10, 10.0)) = 1
		_NScale("Noise Scale", Range(0,10)) = 1
	}

		SubShader
		{
			Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}

			LOD 200
			Pass{
				Name "Empty"
				ZWrite On
				ColorMask 0
			}

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard fullforwardshadows

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			sampler2D _MainTex;
			sampler2D _SpecularMap;
			sampler2D _OcclusionMap;
			sampler2D _BumpMap;
			sampler2D _Noise;
			float _Speed;
			float _NScale;

			struct Input
			{
				float2 uv_MainTex;
				float2 uv_SpecularMap;
				float2 uv_OcclusionMap;
				float2 uv_BumpMap;
				float2 uv2_Noise;
				float2 uv_Normal;
				float3 viewDir;
				float3 worldNormal;
				INTERNAL_DATA
			};

			half _Glossiness;
			half _Metallic;
			fixed4 _Color;
			float4 _RimColor;
			float _RimPower;

			void surf(Input IN, inout SurfaceOutputStandard o)
			{
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				o.Metallic = tex2D(_SpecularMap, IN.uv_SpecularMap).r;
				o.Occlusion = tex2D(_OcclusionMap, IN.uv_OcclusionMap).r;
				o.Smoothness = tex2D(_SpecularMap, IN.uv_SpecularMap).a * _Glossiness;
				

				float3 worldNormal = WorldNormalVector(IN, float3(0, 0, 1));
				//o.Normal = tex2D(_BumpMap, IN.uv_BumpMap);
				// noise thats scaled and moves over time
				float scrollSpeed = _Time.x * _Speed;
				fixed4 n =
					tex2D(_Noise, IN.uv_MainTex * _NScale + scrollSpeed);
				// same noise, but bigger
				fixed4 n2 =
					tex2D(_Noise, IN.uv_MainTex * (_NScale * 0.5) + scrollSpeed);
				// same but smaller
				fixed4 n3 =
					tex2D(_Noise, IN.uv_MainTex * (_NScale * 2) + scrollSpeed);
				// combined
				float combinedNoise = (n.r * n2.r * 2) * n3.r * 2;
				half rim = 1 - saturate(dot(normalize(IN.viewDir + (combinedNoise)), IN.worldNormal));
				rim = pow(rim, 3.0);
				o.Emission = _RimColor.rgb * pow(rim, _RimPower);

			}
			ENDCG
		}
			FallBack "Diffuse"
}
