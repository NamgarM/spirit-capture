using Game;
using Game.Enemies;
using Game.GameStates;
using Game.Player;
using Game.Sound;
using Game.AttackingFeature;
using Game.ActivatePortal;
using Infrastructure.Signals;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Bootstrapinstaller : MonoInstaller, IInitializable
{
    public GameObject MainMenuGameObject;

    public List<Target> enemyTypes;

    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);
        BindInstallerInterfaces();


        // Signals
        DeclarePlayerTookDamageSignal();
        DeclareEnemyTookDamageSignal();
        DeclareEnemyDiedSignal();
        DeclarePlayerDiedSignal();
        // Controllers
        BindPlayerController();
        BindPlayerUIComponent();

        BindEnemiesController();
        BindEnemiesUIComponent();
        BindLightningController();
        BindGameFinisher();
        BindEnemyPoolActivation();


        BindSoundController();
        BindStarter();

        BindActivatePortalController();

        // Pools
        BindEnemyPool();
    }

    private void BindActivatePortalController()
    {
        ActivatePortalController activatePortalController = Container
                           .Instantiate<ActivatePortalController>();
        Container
            .Bind<ActivatePortalController>()
            .FromInstance(activatePortalController)
            .AsSingle();
    }

    private void BindGameFinisher()
    {
        GameFinisher gameFinisher = Container
                           .Instantiate<GameFinisher>();
        Container
            .Bind<GameFinisher>()
            .FromInstance(gameFinisher)
            .AsSingle();
    }

    private void BindLightningController()
    {
        LightningController lightningController = Container
                           .Instantiate<LightningController>();
        Container
            .Bind<LightningController>()
            .FromInstance(lightningController)
            .AsSingle();
    }

    private void DeclarePlayerDiedSignal()
    {
        Container.DeclareSignal<PlayerDiedSignal>();
        Container.BindSignal<PlayerDiedSignal>()
            .ToMethod<GameFinisher>(x => x.FinishGame).FromResolve();
        Container.BindSignal<PlayerDiedSignal>()
            .ToMethod<EnemyPoolActivation>(x => x.DeactivatePool).FromResolve();
        Container.BindSignal<PlayerDiedSignal>()
            .ToMethod<PlayerUIComponent>(x => x.SwitchLostCase).FromResolve();
    }

    private void BindPlayerUIComponent()
    {
        PlayerUIComponent playerUIComponent = Container
                            .Instantiate<PlayerUIComponent>();
        Container
            .Bind<PlayerUIComponent>()
            .FromInstance(playerUIComponent)
            .AsSingle();
    }

    private void BindEnemiesUIComponent()
    {
        EnemiesUIComponent enemiesUIComponent = Container
                           .Instantiate<EnemiesUIComponent>();
        Container
            .Bind<EnemiesUIComponent>()
            .FromInstance(enemiesUIComponent)
            .AsSingle();
    }

    private void BindSoundController()
    {
        SoundController soundController = Container
                           .Instantiate<SoundController>();
        Container
            .Bind<SoundController>()
            .FromInstance(soundController)
            .AsSingle();
    }

    private void BindStarter()
    {
        GameBoostrap gameBoostrap = Container
                           .Instantiate<GameBoostrap>();
        Container
            .Bind<GameBoostrap>()
            .FromInstance(gameBoostrap)
            .AsSingle();
    }

    private void BindEnemyPoolActivation()
    {
        EnemyPoolActivation enemyPoolActivation = Container
                            .Instantiate<EnemyPoolActivation>();
        Container
            .Bind<EnemyPoolActivation>()
            .FromInstance(enemyPoolActivation)
            .AsSingle();
    }

    private void DeclareEnemyDiedSignal()
    {
        Container.DeclareSignal<EnemyDiedSignal>();
        Container.BindSignal<EnemyDiedSignal>()
            .ToMethod<EnemyPoolActivation>(x => x.ActivateEnemy).FromResolve();
        Container.BindSignal<EnemyDiedSignal>()
            .ToMethod<ActivatePortalController>(x => x.ActivatePortal).FromResolve();
    }

    private void BindInstallerInterfaces()
    {
        Container
                    .BindInterfacesTo<Bootstrapinstaller>()
                    .FromInstance(this)
                    .AsSingle();
    }

    private void BindEnemyPool()
    {
        Container
            .Bind<IEnemyPool>()
            .To<EnemyPool>()
            .AsSingle();
    }

    private void BindPlayerController()
    {
        PlayerController playerController = Container
                            .Instantiate<PlayerController>();
        Container
            .Bind<PlayerController>()
            .FromInstance(playerController)
            .AsSingle();
    }

    private void DeclarePlayerTookDamageSignal()
    {
        Container.DeclareSignal<PlayerTookDamageSignal>();
        Container.BindSignal<PlayerTookDamageSignal>()
            .ToMethod<PlayerController>(x => x.TakeDamage).FromResolve();
        Container.BindSignal<PlayerTookDamageSignal>()
            .ToMethod<PlayerUIComponent>(x => x.UpdateView).FromResolve();
    }

    private void DeclareEnemyTookDamageSignal()
    {
        Container.DeclareSignal<EnemyTookDamageSignal>();
        Container.BindSignal<EnemyTookDamageSignal>()
            .ToMethod<EnemiesController>(x => x.TakeDamage).FromResolve();
        Container.BindSignal<EnemyTookDamageSignal>()
            .ToMethod<EnemiesUIComponent>(x => x.UpdateView).FromResolve();
    }

    private void BindEnemiesController()
    {
        EnemiesController enemiesController = Container
                            .Instantiate<EnemiesController>();
        Container
            .Bind<EnemiesController>()
            .FromInstance(enemiesController)
            .AsSingle();
    }

    private void BindEnemiesView()
    {
        EnemiesView enemiesView = Container
                            .Instantiate<EnemiesView>();
        Container
            .Bind<EnemiesView>()
            .FromInstance(enemiesView)
            .AsSingle();
    }

    private void BindMainMenuView()
    {
        MainMenuView mainMenuView = Container
                    .Instantiate<MainMenuView>();
        Container
            .Bind<MainMenuView>()
            .FromInstance(mainMenuView)
            .AsSingle();
    }

    public void Initialize()
    {
        var enemyPool = Container.Resolve<IEnemyPool>();
        enemyPool.Load();
        foreach (Target enemyType in enemyTypes)
        {
            enemyPool.Create(enemyType.EnemiesTypes, enemyType.gameObject.transform.rotation);
        }
        enemyPool.Deactivate();
    }
}