﻿using Game.Enemies;
using System.Collections.Generic;
using UnityEngine;

internal interface IEnemyPool
{
    public List<GameObject> pooledEnemies { get; set; }
    public void Load();
    public void Create(EnemiesTypes enemyType, Quaternion rotation);

    public GameObject GetPooledGameItems();
    public void Deactivate();
}