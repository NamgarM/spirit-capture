using UnityEngine;

namespace Infrastructure.Signals
{
    public class PlayerTookDamageSignal
    {
        public EnemiesModel EnemiesModel;
    }
}
