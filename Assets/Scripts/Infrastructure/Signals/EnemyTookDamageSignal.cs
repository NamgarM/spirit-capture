using UnityEngine;
using Zenject;

namespace Infrastructure.Signals
{
    public class EnemyTookDamageSignal
    {
        public EnemiesView EnemiesView;
        public float DamageAmount;
        public GameObject EnemyObject;
    }
}
