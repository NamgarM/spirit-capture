using Game.Sound;
using Infrastructure.Signals;
using UnityEngine;
using Zenject;

namespace Game.GameStates
{
    public class GameBoostrap
    {
        private SignalBus _signalBus;
        private SoundController _soundController;

        public GameBoostrap(SignalBus signalBus, SoundController soundController)
        {
            _signalBus = signalBus;
            _soundController = soundController;
        }

        public void Play(GameObject lightningMarker, GameObject gamePlayController)
        {
            lightningMarker.SetActive(true);
            gamePlayController.SetActive(true);
            // Activate item
            _signalBus.Fire(new EnemyDiedSignal());
            _soundController.Stop("MainMenuSound");
            if (_soundController.IsPlaying("MainMenuSound") == false)
            {
                _soundController.Play("GameplaySound");
            }
        }
    }
}
