namespace Game.GameStates
{
    public enum GameStates : byte
    {
        Menu = 0,
        GameStarted = 1,
        Lost = 2,
        Won = 3,
        Paused = 4,
        Finished = 5
    }
}
