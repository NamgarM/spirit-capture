using Game.AttackingFeature;
using Game.Enemies;
using Infrastructure.Signals;

namespace Game.GameStates
{
    public class GameFinisher
    {
        private EnemiesController _enemiesController;
        private LightningController _lightningController;
        public GameFinisher(EnemiesController enemiesController,
            LightningController lightningController)
        {
            _enemiesController = enemiesController;
            _lightningController = lightningController;
        }

        public void FinishGame(PlayerDiedSignal playerDiedSignal)
        {
            //_enemyPool.Deactivate();
            _lightningController.UpdateLightningStatus(false);
        }
    }
}
