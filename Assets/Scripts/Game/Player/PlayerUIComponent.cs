using Infrastructure.Signals;

namespace Game.Player
{
    public class PlayerUIComponent
    {
        private PlayerView _playerView;

        public void SetPlayerView(PlayerView playerView)
        {
            _playerView = playerView;
        }

        public void UpdateView(PlayerTookDamageSignal playerTookDamageSignal)
        {
            _playerView.PlayerHealthbarFillingImage.fillAmount =
                _playerView.PlayersHealth / _playerView.PlayersFullHealth;
        }

        public void SwitchLostCase(PlayerDiedSignal playerDiedSignal)
        {
            _playerView.Youlost_Txt.gameObject.SetActive(true);
        }
    }
}
