using UnityEngine;
using Zenject;
using UnityEngine.UI;

namespace Game.Player
{
    public class PlayerView : MonoBehaviour
    {
        public float PlayersHealth { get; set; }
        public float PlayersFullHealth { get; private set; }
        public Image PlayerHealthbarFillingImage;
        public Text Youlost_Txt;
        //public float PlayersDamage { get; set; }

        private PlayerController _playerController;
        private PlayerUIComponent _playerUIComponent;

        [Inject]
        public void Construct(PlayerController playerController, PlayerUIComponent playerUIComponent)
        {
            _playerController = playerController;
            _playerController.SetPlayersView(this);

            _playerUIComponent = playerUIComponent;
            _playerUIComponent.SetPlayerView(this);
        }

        void Awake()
        {
            PlayersHealth = 100f;
            PlayersFullHealth = PlayersHealth;
        }
    }
}
