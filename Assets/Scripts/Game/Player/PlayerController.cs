using Infrastructure.Signals;
using UnityEngine;
using Zenject;

namespace Game.Player
{
    public class PlayerController
    {
        public PlayerView PlayersView { get; private set; }
        private SignalBus _signalBus;

        public PlayerController(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        public void SetPlayersView(PlayerView playersView)
        {
            PlayersView = playersView;
        }

        public void TakeDamage(PlayerTookDamageSignal playerTookDamageSignal)
        {
            PlayersView.PlayersHealth -= playerTookDamageSignal.EnemiesModel.EnemiesDamage;
            if (PlayersView.PlayersHealth <= 0f)
                _signalBus.Fire(new PlayerDiedSignal());
        }
    }
}
