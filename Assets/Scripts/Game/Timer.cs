using Game.Enemies;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Timer : MonoBehaviour
{
    // Timer
    private float _targetTime = 20f;
    private EnemiesController _enemiesController;
    private float _speed = 1f;

    [Inject]
    public void Construct(EnemiesController enemiesController)
    {
        _enemiesController = enemiesController;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Timer
        _targetTime -= Time.deltaTime;
        Debug.Log(_targetTime);
        if (_targetTime <= 0.0f)
        {
            _speed += 0.25f;
            _enemiesController.SetSpeed(_speed);
            _targetTime = 15f;
        }
    }
}
