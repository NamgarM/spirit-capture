using Game.ActivatePortal;
using UnityEngine;
using Zenject;

public class ActivatePortalView : MonoBehaviour
{
    private ActivatePortalController _activatePortalController;
    public float _targetTime = 0.8f;
    [Inject]
    public void Construct(ActivatePortalController activatePortalController)
    {
        _activatePortalController = activatePortalController;
        _activatePortalController.SetActivatePortalView(this);
    }
    // Update is called once per frame
    void Update()
    {
        // Timer
        _targetTime -= Time.deltaTime;
        if (_targetTime <= 0.0f)
        {
            _activatePortalController.ActivatePortal();
            _targetTime = 0.8f;
        }
    }
}
