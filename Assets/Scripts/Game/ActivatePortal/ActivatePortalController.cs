namespace Game.ActivatePortal
{
    public class ActivatePortalController
    {
        private EnemyPoolActivation _enemyPoolActivation;
        private ActivatePortalView _activatePortalView;

        public ActivatePortalController(EnemyPoolActivation enemyPoolActivation)
        {
            _enemyPoolActivation = enemyPoolActivation;
        }
        public void ActivatePortal()
        {
            _activatePortalView.transform.position =
                _enemyPoolActivation._gameItem.transform.position;
            _activatePortalView.gameObject.SetActive(!_activatePortalView.gameObject.activeInHierarchy);


        }

        internal void SetActivatePortalView(ActivatePortalView activatePortalView)
        {
            _activatePortalView = activatePortalView;
        }
    }
}
