using System;
using Game;
using Game.Enemies;

[Serializable]
public class EnemiesModel : IEnemiesModel
{
    public EnemiesTypes EnemiesTypes { get; set; }
    public float EnemiesHealth { get; set; }
    public float EnemiesDamage { get; set; }
    public float EnemiesStartingHealth { get; private set; }

    public EnemiesModel(EnemiesTypes enemiesTypes = 0,
        float enemiesHealth = 0f, float enemiesDamage = 0f, float enemiesStartingHealth = 0f)
    {
        EnemiesTypes = enemiesTypes;
        EnemiesHealth = enemiesHealth;
        EnemiesDamage = enemiesDamage;
        EnemiesStartingHealth = enemiesStartingHealth;
    }

    public void TakeDamage()
    {

    }
    public void MakeDamage()
    {

    }
    public void Die()
    {

    }
    public void Move()
    {

    }
}
