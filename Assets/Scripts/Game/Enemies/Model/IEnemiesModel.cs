using Game.Enemies;

namespace Game
{
    interface IEnemiesModel
    {
        public EnemiesTypes EnemiesTypes { get; set; }
        public float EnemiesHealth { get; set; }
        public float EnemiesDamage { get; set; }
        void TakeDamage();
        void MakeDamage();
        void Die();
        void Move();
    }
}

