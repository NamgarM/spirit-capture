using Infrastructure.Signals;

namespace Game.Enemies
{
    public class EnemiesUIComponent
    {
        public void UpdateView(EnemyTookDamageSignal enemyTookDamageSignal)
        {
            enemyTookDamageSignal.EnemiesView
                .EnemyHealthbarFill.fillAmount = 
                enemyTookDamageSignal.EnemiesView
                .CurrentEnemiesModel.EnemiesHealth /
                enemyTookDamageSignal.EnemiesView.CurrentEnemiesModel.EnemiesStartingHealth;
            if (enemyTookDamageSignal.EnemyObject.activeInHierarchy == false)
                enemyTookDamageSignal.EnemiesView.EnemyHealthbarFill.fillAmount = 1f;
        }
    }
}
