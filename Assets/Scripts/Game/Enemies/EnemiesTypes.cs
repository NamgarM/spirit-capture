namespace Game.Enemies
{
    public enum EnemiesTypes : byte
    {
        SpiritOfDespair = 0,
        SpiritOfTrickery = 1,
        SpiritOfVengence = 2
    }
}
