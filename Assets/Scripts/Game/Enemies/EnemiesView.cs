using Game;
using Game.Enemies;
using Game.Player;
using Game.Sound;
using Infrastructure.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class EnemiesView : MonoBehaviour
{
    private EnemiesCharacteristics _enemiesCharacteristics;
    private EnemiesController _enemiesController;
    public EnemiesModel CurrentEnemiesModel { get; private set; }
    private SignalBus _signalBus; 

    private PlayerController _playerController;
    private SoundController _soundController;

    // Movement
    [HideInInspector]
    public float _speed = 7f;
    public Image EnemyHealthbarFill;

    [Inject]
    public void Construct(EnemiesController enemiesController,
        SignalBus signalBus, PlayerController playerController,
        SoundController soundController)
    {
        _enemiesController = enemiesController;
        _signalBus = signalBus;
        _playerController = playerController;
        _soundController = soundController;
        _enemiesController.SetEnemiesView(this.gameObject, signalBus, soundController, this, 3f);
    }

    // Start is called before the first frame update
    void Awake()
    {
        _enemiesCharacteristics = new EnemiesCharacteristics();
        // Choose type
        var enemyType = this.GetComponent<Target>().EnemiesTypes;
        switch (enemyType)
        {
            case EnemiesTypes.SpiritOfDespair:
                CurrentEnemiesModel = _enemiesCharacteristics.EnemiesModel[0];
                break;
            case EnemiesTypes.SpiritOfTrickery:
                CurrentEnemiesModel = _enemiesCharacteristics.EnemiesModel[1];
                break;
            case EnemiesTypes.SpiritOfVengence:
                CurrentEnemiesModel = _enemiesCharacteristics.EnemiesModel[2];
                break;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.name == "Main Camera")
        {
            _enemiesController
                .MakeDamage(this.gameObject, CurrentEnemiesModel, _signalBus);
        }
    }

    // Update is called once per frame
    void Update()
    {
        float step = _speed * Time.deltaTime;
        if (this.transform.position !=
            _playerController.PlayersView.transform.position)
        {
            this.transform.position = Vector3.MoveTowards(
                this.transform.position,
                 _playerController.PlayersView.transform.position,
                 _speed);
            _soundController.Play("SpiritChaseSound");
        }
        else
        {
            _signalBus.Fire(new EnemyDiedSignal());
            CurrentEnemiesModel.EnemiesHealth =
                CurrentEnemiesModel.EnemiesStartingHealth;
            this.gameObject.SetActive(false);
        }
    }
}
