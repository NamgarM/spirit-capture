using Game.Enemies;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameItemPool : MonoBehaviour
{
    public static GameItemPool Pool;
    public List<GameObject> pooledGameItems;
    public List<GameObject> gameItemToPool;
    private int amountToPool = 9;
    private Vector3 initialGameItemPos = new Vector3(-2f, 0f, 7f);
    private int randomInitialPosIndex;
    public GameObject _gameItem;
    public bool isAnyGameItemActive = false;
    private EnemiesController _enemiesController;

    [Inject]
    public void Construct(EnemiesController enemiesController)
    {
        _enemiesController = enemiesController;
    }

    private void Awake()
    {
        Pool = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        /*
        pooledGameItems = new List<GameObject>();
        GameObject currentGameItem;
        for(int counter = 0; counter < amountToPool; counter++)
        {
            currentGameItem = Instantiate(gameItemToPool[counter]);
            currentGameItem.SetActive(false);
            pooledGameItems.Add(currentGameItem);
        }*/
    }

    public GameObject GetPooledGameItems()
    {
        List<GameObject> inactiveGameItem =
            pooledGameItems.FindAll(go => !go.activeInHierarchy);
        // If list is not empty, check random game otem
        return inactiveGameItem.Count > 0 ?
            inactiveGameItem[Random
            .Range(0, inactiveGameItem.Count)] :
            null;
    }

    public void ActivateItem()
    {
        _gameItem =
            GameItemPool.Pool.GetPooledGameItems();
        if (_gameItem != null)
        {
            // position
            randomInitialPosIndex = Random.Range(0, 4);
            _gameItem.transform.position =
                new Vector3(initialGameItemPos.x +
                1f * randomInitialPosIndex,
                initialGameItemPos.y,
                initialGameItemPos.z);
            Debug.Log(_gameItem.transform.position);
            // activation state
            _gameItem.SetActive(true);
        }
    }

    void Update()
    {
        //EventManager.OnGameItemDeactivated += SpawnItem();
        if (Input.GetKeyDown(KeyCode.K))//(!isAnyGameItemActive)
        {
            _gameItem =
            GameItemPool.Pool.GetPooledGameItems();
            if (_gameItem != null)
            {
                // position
                randomInitialPosIndex = Random.Range(0, 4);
                _gameItem.transform.position =
                    new Vector3(initialGameItemPos.x +
                    1f * randomInitialPosIndex,
                    initialGameItemPos.y,
                    initialGameItemPos.z);
                Debug.Log(_gameItem.transform.position);
                // activation state
                _gameItem.SetActive(true);
                isAnyGameItemActive = true;
            }
        }
    }
}
