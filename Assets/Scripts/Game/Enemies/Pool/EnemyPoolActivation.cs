using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EnemyPoolActivation
{
    public List<GameObject> pooledEnemies = new List<GameObject>();
    public GameObject _gameItem;
    private Vector3 initialEnemyPos = new Vector3(-2f, 0f, 7f);
    private int randomInitialPosIndex;

    public void ActivateEnemy()
    {
        _gameItem = GetPooledGameItems();
        if (_gameItem != null)
        {
            // position
            randomInitialPosIndex = Random.Range(0, 4);
            _gameItem.transform.position =
                new Vector3(initialEnemyPos.x +
                1f * randomInitialPosIndex,
                initialEnemyPos.y,
                initialEnemyPos.z);
            // activation state
            _gameItem.SetActive(true);
        }
    }

    public void DeactivatePool()
    {
        _gameItem.GetComponent<EnemiesView>().enabled = false;
        _gameItem.SetActive(false);
    }

    public GameObject GetPooledGameItems()
    {
        List<GameObject> inactiveGameItem =
            pooledEnemies.FindAll(go => !go.activeInHierarchy);
        // If list is not empty, check random game otem
        return inactiveGameItem.Count > 0 ?
            inactiveGameItem[Random
            .Range(0, inactiveGameItem.Count)] :
            null;
    }
}
