using Game.Enemies;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Infrastructure.Signals;

public class EnemyPool : IEnemyPool
{
    public static GameItemPool Pool;
    public List<GameObject> pooledGameItems;
    public List<GameObject> gameItemToPool;
    private int amountToPool = 9;
    private Vector3 initialEnemyPos = new Vector3(-2f, -1f, 7f);
    private int randomInitialPosIndex;
    public GameObject _gameItem;
    public bool isAnyGameItemActive = false;
    public List<GameObject> pooledEnemies { get; set; }

    private readonly DiContainer _diContainer;
    private SignalBus _signalBus;
    private EnemyPoolActivation _enemyPoolActivation;

    private const string SpiritOfDespairPath = "Prefabs/SpiritOfDespair";
    private const string SpiritOfTrickeryPath = "Prefabs/SpiritOfTrickery";
    private const string SpiritOfVengencePath = "Prefabs/SpiritOfVengence";
    private Object _spiritOfDespairPrefab;
    private Object _spiritOfTrickeryPrefab;
    private Object _spiritOfVengencePrefab;

    public EnemyPool(DiContainer diContainer, SignalBus signalBus, EnemyPoolActivation enemyPoolActivation)
    {
        _diContainer = diContainer;
        pooledEnemies = new List<GameObject>();
        _signalBus = signalBus;
        _enemyPoolActivation = enemyPoolActivation;
    }

    public void Create(EnemiesTypes enemyType, Quaternion rotation)
    {
        switch (enemyType)
        {
            case EnemiesTypes.SpiritOfDespair:
                _enemyPoolActivation.pooledEnemies.Add(_diContainer.InstantiatePrefab(_spiritOfDespairPrefab, initialEnemyPos, rotation, null));
                break;
            case EnemiesTypes.SpiritOfTrickery:
                _enemyPoolActivation.pooledEnemies.Add(_diContainer.InstantiatePrefab(_spiritOfTrickeryPrefab, initialEnemyPos, rotation, null));
                break;
            case EnemiesTypes.SpiritOfVengence:
                _enemyPoolActivation.pooledEnemies.Add(_diContainer.InstantiatePrefab(_spiritOfVengencePrefab, initialEnemyPos, rotation, null));
                break;
        }
    }

    public GameObject GetPooledGameItems()
    {
        List<GameObject> inactiveGameItem =
            pooledEnemies.FindAll(go => !go.activeInHierarchy);
        // If list is not empty, check random game otem
        return inactiveGameItem.Count > 0 ?
            inactiveGameItem[Random
            .Range(0, inactiveGameItem.Count)] :
            null;
    }

    public void Load()
    {
        _spiritOfDespairPrefab = Resources.Load(SpiritOfDespairPath);
        _spiritOfTrickeryPrefab = Resources.Load(SpiritOfTrickeryPath);
        _spiritOfVengencePrefab = Resources.Load(SpiritOfVengencePath);
    }

    public void Deactivate()
    {
        if (pooledEnemies != null)
        {
            foreach (GameObject pooledEnemy in _enemyPoolActivation.pooledEnemies)
            {
                pooledEnemy.SetActive(false);
            }
        }
    }
}
