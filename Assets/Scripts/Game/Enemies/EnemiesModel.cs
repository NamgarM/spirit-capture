using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Enemies
{
    public class EnemiesCharacteristics
    {
        public List<EnemiesModel> EnemiesModel = new List<EnemiesModel>();

        public EnemiesCharacteristics()
        {
            EnemiesModel.Add(
                new EnemiesModel(EnemiesTypes.SpiritOfDespair, 10f, 5f, 10f));
            EnemiesModel.Add(
                new EnemiesModel(EnemiesTypes.SpiritOfTrickery, 20f, 10f, 20f));
            EnemiesModel.Add(
                new EnemiesModel (EnemiesTypes.SpiritOfVengence, 30f, 20f, 30f));
        }
    }
}
