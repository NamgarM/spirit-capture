using Game.Sound;
using Infrastructure.Signals;
using UnityEngine;
using Zenject;

namespace Game.Enemies
{
    public class EnemiesController
    {
        private GameObject _currentEnemy;
        private SignalBus _signalBus;
        private SoundController _soundController;
        private EnemiesView _enemiesView;

        public void SetEnemiesView(GameObject currentEnemy, SignalBus signalBus,
            SoundController soundController, EnemiesView enemiesView, float speed)
        {
            _currentEnemy = currentEnemy;
            _signalBus = signalBus;
            _soundController = soundController;
        }

        public void TakeDamage(EnemyTookDamageSignal enemyTookDamageSignal)
        {
            enemyTookDamageSignal.EnemiesView.CurrentEnemiesModel.EnemiesHealth -= enemyTookDamageSignal.DamageAmount;
            // Death
            if (enemyTookDamageSignal.EnemiesView.CurrentEnemiesModel.EnemiesHealth <= 0f)
            {
                Died(enemyTookDamageSignal.EnemyObject);
                _soundController.Play("SpiritDieSound");
            }
        }

        public void MakeDamage(GameObject currentEnemy,
            global::EnemiesModel CurrentEnemiesSettings, SignalBus signalBus)
        {
            _soundController.Play("SpiritAttackSound");
            signalBus.Fire(new PlayerTookDamageSignal()
            {
                EnemiesModel = CurrentEnemiesSettings
            });
            Died(currentEnemy);
        }

        public void Died(GameObject currentEnemy)
        {
            _signalBus.Fire(new EnemyDiedSignal());
            currentEnemy.SetActive(false);
        }

        public void Deactivate()
        {
            if (_currentEnemy.activeInHierarchy == true)
            {
                _currentEnemy.SetActive(false);
            }
        }

        public void SetSpeed(float speed)
        {
            _enemiesView._speed = speed;
        }
    }
}
