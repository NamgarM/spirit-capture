using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;
using Zenject;

namespace Game.AttackingFeature
{
    public class LightningView : MonoBehaviour
    {
        public GameObject LightningMarker;
        private LightningController _lightningController;

        [Inject]
        public void Construct(LightningController lightningController)
        {
            _lightningController = lightningController;
        }

        private void Start()
        {
            _lightningController.SetLightningView(this);
            //LightningMarker.GetComponent<Renderer>().enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            _lightningController.MarkerFollowsHand(LightningMarker);
        }
    }
}
