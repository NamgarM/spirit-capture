using Infrastructure.Signals;
using UnityEngine;
using Zenject;

namespace Game
{
    public class ShootingController : MonoBehaviour
    {
        private float _damage = 5f;
        private Camera fpsCam;
        public GameObject LightningParticleEffect;

        private float _targetTime = 1.0f;
        private bool isLightningCasted = false;

        private SignalBus _signalBus;

        [Inject]
        public void Constructor(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        // Start is called before the first frame update
        void Start()
        {
            fpsCam = Camera.main;
        }

        private void Update()
        {
            CheckTimer();
        }

        private void CheckTimer()
        {
            if (isLightningCasted == true)
            {
                _targetTime -= Time.deltaTime;
                if (_targetTime <= 0.0f)
                {
                    LightningParticleEffect.SetActive(!isLightningCasted);
                    isLightningCasted = !isLightningCasted;
                    _targetTime = 1.0f;
                }
            }
        }

        public void Shoot()
        {
            RaycastHit hitInfo;
            if (Physics
                .Raycast(fpsCam.transform.position,
                fpsCam.transform.forward, out hitInfo))
            {
                var enemiesView = hitInfo.transform.GetComponent<EnemiesView>();
                if (enemiesView != null)
                {
                    _signalBus.Fire(new EnemyTookDamageSignal()
                    {
                        EnemiesView = enemiesView,
                        DamageAmount = _damage,
                        EnemyObject = hitInfo.transform.gameObject
                    });
                }
            }
        }

        public void SwitchLightning()
        {
            isLightningCasted = !isLightningCasted;
            LightningParticleEffect.SetActive(true);
        }
    }
}
