using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.AttackingFeature
{
    public class LightningController
    {
        private LightningView _lightningView;
        MixedRealityPose pose;
        public void SetLightningView(LightningView lightningView)
        {
            _lightningView = lightningView;
        }

        public void UpdateLightningStatus(bool isActivated)
        {
            _lightningView.gameObject.SetActive(isActivated);
        }

        internal void MarkerFollowsHand(GameObject lightningMarker)
        {

            if (HandJointUtils.TryGetJointPose(TrackedHandJoint.IndexTip, Handedness.Right, out pose))
            {
                lightningMarker.GetComponent<Renderer>().enabled = true;
                lightningMarker.transform.position =
                    pose.Position + new Vector3(-0.03f, 0f, 0.03f);
            }
        }
    }
}
