using DG.Tweening;
using Game.GameStates;
using Game.Sound;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GuideAnimation : MonoBehaviour
{
    private Material _guideMaterial;
    private Animator _guideAnimator;
    private int _fadeIdleHash = Animator.StringToHash("FadeIdle");
    private int _talkHash = Animator.StringToHash("Talk");
    private int _lightningTutorialTalkHash = Animator.StringToHash("LightningTutorialTalk");
    private int _idleHash = Animator.StringToHash("Idle");
    private SoundController _soundController;

    private bool _isFired = false;
    private GameBoostrap _gameBoostrap;
    public GameObject LightningMarker;
    public GameObject GamePlayController;

    [Inject]
    public void Construct(GameBoostrap gameBoostrap, SoundController soundController)
    {
        _gameBoostrap = gameBoostrap;
        _soundController = soundController;
    }

    // Start is called before the first frame update
    void Start()
    {
        _guideAnimator = GetComponent<Animator>();
        _guideMaterial = this.gameObject
            .GetComponentInChildren<SkinnedMeshRenderer>().material;
        _guideMaterial
            .DOColor(new Color(1f, 1f, 1f, 1f), 2f);
    }

    // Update is called once per frame
    void Update()
    {
        AnimatorStateInfo animatorStateInfo = _guideAnimator
            .GetCurrentAnimatorStateInfo(0);
        if (animatorStateInfo.shortNameHash == _idleHash && animatorStateInfo.normalizedTime > 1)
        {
            _guideAnimator.SetBool("DoGuideDub", true);
            _soundController.Play("GuideIntro");
        }
        
        if (animatorStateInfo.shortNameHash == _talkHash && !_soundController.IsPlaying("GuideIntro"))
        {
            _guideAnimator.SetBool("DoGuideLightningTutorial", true);
            _soundController.Play("LightningTutorial");
        }

        if (animatorStateInfo.shortNameHash == _lightningTutorialTalkHash && !_soundController.IsPlaying("LightningTutorial"))
        {
            _guideAnimator.SetBool("DoFade", true);
        }

        if (animatorStateInfo.shortNameHash == _fadeIdleHash)
        {
            _guideMaterial.DOFade(0f, 2f).OnComplete(() =>
            {
                if (_isFired == false)
                {
                    _gameBoostrap.Play(LightningMarker, GamePlayController);
                    _isFired = true;
                }
                this.gameObject.SetActive(false);
            }); ;
        }
    }
}
