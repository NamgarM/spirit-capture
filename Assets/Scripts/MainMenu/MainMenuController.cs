using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace MainMenu
{
    public class MainMenuController : MonoBehaviour
    {
        public GameObject Play_Btn;
        public GameObject Quit_Btn;
        public List<GameObject> levels;

        // Animation
        public Ease animEase;
        public Vector3[] Path;
        public PathType PathType;
        public PathMode PathMode;

        private MainMenuView MainMenuView;
        private void Construct()
        {

        }

        public void ChoosePlayButton()
        {
            foreach (GameObject level in levels)
            {
                level.gameObject.SetActive(true);
            }
            this.gameObject.GetComponent<MainMenuAnimation>().PlayLevelChoosingAnimation();
        }

        public void ChooseLevel(GameObject chosenLevel)
        {
            foreach (GameObject level in levels)
            {
                if (chosenLevel != level)
                    level.gameObject.SetActive(false);
            }
        }
    }
}
