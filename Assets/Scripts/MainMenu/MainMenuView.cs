using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MainMenuView : MonoBehaviour
{
    public GameObject MainMenu;
    public GameObject Play_Btn;
    public GameObject Quit_Btn;
    public List<GameObject> levels;

    // Animation
    private float startingAnimDuration = 5f;
    public Ease animEase;
    public Vector3[] Path;
    public PathType PathType;
    public PathMode PathMode;

    private void Awake()
    {
        MainMenu = this.gameObject;
    }
}
