using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class MainMenuAnimation : MonoBehaviour
{
    public GameObject Play_Btn;
    public GameObject Quit_Btn;
    public List<GameObject> levels;
    private List<Material> levelsMaterial = new List<Material>();

    // Animation
    private float startingAnimDuration = 5f;
    private Ease animEase = Ease.OutQuad;
    private Vector3[] Path;
    private Vector3 finishPos;
    private Vector3 startingPos;
    public PathType PathType = PathType.CatmullRom;
    public PathMode PathMode = PathMode.Full3D;

    public GameObject guide;

    // Start is called before the first frame update
    void Start()
    {
        PlayMenuAnimation(Quit_Btn, Vector3.right, 1);
        PlayMenuAnimation(Play_Btn, Vector3.left, -1);
    }

    private void PlayMenuAnimation
        (GameObject button, Vector3 middlePointPos, int indexPointPos)
    {
        startingPos =
            new Vector3(indexPointPos * 0.075f, -0.2f, -0.5f);
        finishPos =
            new Vector3(indexPointPos * 0.075f, -0.2f, 0.5f);
        Path =
            new[] { startingPos, middlePointPos, finishPos };
        button.transform
            .DOPath(Path, startingAnimDuration, PathType, PathMode, 10)
            .SetEase(animEase);
    }

    public void PlayLevelChoosingAnimation()
    {
        // Material 
        Material quitButtonMaterial = Quit_Btn.GetComponent<MeshRenderer>().material;
        Material playButtonMaterial = Play_Btn.GetComponent<MeshRenderer>().material;
        quitButtonMaterial.DOFade(0f, 0.5f);
        playButtonMaterial.DOFade(0f, 0.5f);
        // Text
        Quit_Btn.GetComponentInChildren<Text>().DOFade(0f, 0.5f);
        Play_Btn.GetComponentInChildren<Text>().DOFade(0f, 0.5f);

        // Level animation
        Sequence levelChoosingSequence = DOTween.Sequence();
        levelChoosingSequence.Append(quitButtonMaterial.DOFade(0f, 0.5f));
        
        //
        for(int i = 0; i < levels.Count; i++)
        {
            levelsMaterial.Add(levels[i].GetComponent<MeshRenderer>().material);
            levelChoosingSequence.Insert(0.5f, levelsMaterial[i].DOColor(new Color(0f, 0f, 0f, 1f), 0.5f));
        }
        levelChoosingSequence.OnComplete(() =>
        {
            Play_Btn.gameObject.SetActive(false);
            Quit_Btn.gameObject.SetActive(false);
        });
    }

    public void SwitchOffLevels()
    {
        Sequence levelChoosingSequence = DOTween.Sequence();
        for (int i = 0; i < levelsMaterial.Count; i++)
        {
            levelChoosingSequence.Insert(0f, levelsMaterial[i].DOFade(0f, 0.5f));
            levelChoosingSequence.Insert(0f, levels[i].GetComponentInChildren<Text>().DOFade(0f, 0.5f));
        }

        levelChoosingSequence.OnComplete(() =>
        {
            for (int i = 0; i < levels.Count; i++)
            {
                levels[i].SetActive(false);
            }
            guide.gameObject.SetActive(true);
        });
    }
}
