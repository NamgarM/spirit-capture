using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game.Sound
{
    public class SoundView : MonoBehaviour
    {
        public List<SoundSettings> Sounds;
        private SoundController _soundController;

        [Inject]
        public void Construct(SoundController soundController)
        {
            _soundController = soundController;
            _soundController.SetSoundView(this);
        }

        // Start is called before the first frame update
        void Awake()
        {
            foreach (SoundSettings s in Sounds)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.clip = s.clip;
                s.source.pitch = s.pitch;
                s.source.volume = s.pitch;
                s.source.playOnAwake = false;
            }
        }

        private void Start()
        {
            _soundController.Play("MainMenuSound");
        }

        public void Play(string soundName)
        {
            _soundController.Play(soundName);
        }
        public void Stop(string soundName)
        {
            _soundController.Stop(soundName);
        }
    }
}
