namespace Game.Sound
{
    public class SoundController
    {
        private SoundView _soundView;

        public void SetSoundView(SoundView soundView)
        {
            _soundView = soundView;
        }

        public void Play(string soundName)
        {
            foreach (SoundSettings sound in _soundView.Sounds)
            {
                if (sound.name == soundName)
                {
                    sound.source.Play();
                    sound.source.volume = sound.volume;
                    //sound.source.enabled = true;
                }
            }
        }

        public void Stop(string soundName)
        {
            foreach (SoundSettings sound in _soundView.Sounds)
            {
                if (sound.name == soundName)
                {
                    sound.source.Stop();
                    sound.source.mute = true;
                }
            }
        }

        public bool IsPlaying(string soundName)
        {
            foreach (SoundSettings sound in _soundView.Sounds)
            {
                if (sound.name == soundName)
                    return sound.source.isPlaying;
            }
            return false;
        }
    }
}
