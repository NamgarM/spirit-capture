using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public List<SoundSettings> Sounds;
    // Start is called before the first frame update
    void Awake()
    {
        foreach (SoundSettings s in Sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.pitch = s.pitch;
            s.source.volume = s.pitch;
        }
        Play("MainMenuSound");
    }

    public void Play(string soundName)
    {
        foreach (SoundSettings sound in Sounds)
        {
            if (sound.name == soundName)
                sound.source.Play();
        }
    }

    public void Stop(string soundName)
    {
        foreach (SoundSettings sound in Sounds)
        {
            if (sound.name == soundName)
                sound.source.Stop();
        }
    }
}
